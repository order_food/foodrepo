package com.foodbytes.service;

import java.util.List;

import com.foodbytes.model.Order;

/**
 * @author subdas3
 *
 */
public interface IOrderService {
	public Order addToCart(int customerId,int itemId, int quantity);
	public List<Order> gettOrdersByCustomerId(int customerId);
	public Order updateCart(int cartId,int quantity);
	public Order deleteOrderById(int orderId);
	public Order deleteCartById(int cartId);
	
	public Order getOrderById(int orderId);
	public Order checkOut(int customerId,String address);
	

}
