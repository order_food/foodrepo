package com.foodbytes.service;


import java.awt.print.Book;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foodbytes.feignclient.IOrderClient;
import com.foodbytes.model.Cart;
import com.foodbytes.model.Hotel;
import com.foodbytes.model.Menus;
import com.foodbytes.model.Order;
import com.foodbytes.repository.CartRepository;
import com.foodbytes.repository.OrderRepository;

/**
 * @author subdas3
 *
 */
@Service
public class OrderServiceImpl implements IOrderService{
	
	@Autowired
	CartRepository cartRepository;
	@Autowired
	OrderRepository orderRepository;
	@Autowired
	IOrderClient iOrderClient;
	


	
	@Override
	public Order addToCart(int customerId, int itemId, int quantity) {
	
		Hotel hotel=null;
		Menus menus=iOrderClient.showMenus(itemId);
		hotel=menus.getHotel();
		
		
		
		List<Order> orders=orderRepository.findByStatusAndCustomerId("NEW", customerId);
		Order order = null;
		Cart cart=new Cart();
		if(orders.isEmpty()) {
			order=new Order();
			order.setStatus("NEW");
			order.setCustomerId(customerId);
			order.setHotelId(hotel.getHotelId());
			order.setCartList(new ArrayList<Cart>());
			order=orderRepository.save(order);
			cart.setOrder(order);
			cart.setQuantity(quantity);
			cart.setItemId(itemId);
			cart.setItemName(menus.getItemName());
			cart.setTotal((double) (quantity*menus.getPrice()));
			List<Cart> deltaCart= order.getCartList();
			deltaCart.add(cart);
			order.setCartList(deltaCart);
			orderRepository.save(order);
			
			
			
		}else {
			for(Order order2:orders) {
				order=order2;
			}
			
			cart.setOrder(order);
			cart.setQuantity(quantity);
			cart.setItemId(itemId);
			cart.setItemName(menus.getItemName());
			cart.setTotal((double) (quantity*menus.getPrice()));
			List<Cart> deltaCart= order.getCartList();
			deltaCart.add(cart);
			order.setCartList(deltaCart);
			orderRepository.save(order);
			
			
		}
		
	return order;
		
	}
	
	

	@Override
	public List<Order> gettOrdersByCustomerId(int customerId) {
		// TODO Auto-generated method stub
		return orderRepository.findByCustomerId(customerId);
	}

	@Override
	public Order updateCart(int cartId, int quantity) {
		// TODO Auto-generated method stub
		Cart cart=cartRepository.findById(cartId).get();
		int orderId=cart.getOrder().getOrderId();
		cart.setQuantity(quantity);
		int itemId=cart.getItemId();
		Menus menu=iOrderClient.showMenus(itemId);
		cart.setTotal((double) (menu.getPrice()*quantity));
		cartRepository.save(cart);
		return orderRepository.findById(orderId).get();
		 
	}





	@Override
	public Order getOrderById(int orderId) {
		// TODO Auto-generated method stub
		return orderRepository.findById(orderId).get();
	}

	

	@Override
	public Order deleteCartById(int cartId) {
		// TODO Auto-generated method stub
		Cart cart=cartRepository.findById(cartId).get();
		int orderId=cart.getOrder().getOrderId();
		Order order=orderRepository.findById(orderId).get();
		cartRepository.delete(cart);
		return order;
	}

	@Override
	public Order checkOut(int customerId, String address) {
		// TODO Auto-generated method stub
		Order order=null;
		for(Order order1:orderRepository.findByStatusAndCustomerId("NEW", customerId))
		order=order1;
		int subTotal=0;
		
		for(Cart cart:order.getCartList())
			subTotal+=cart.getTotal();
		order.setAddress(address);
		order.setSubTotal(subTotal);
		order.setStatus("ORDERED");
		orderRepository.save(order);
		return order;
	}

	@Override
	public Order deleteOrderById(int orderId) {
		// TODO Auto-generated method stub
		Order order=null;
		order=orderRepository.findById(orderId).get();
		order.setStatus("CANCELLED");
		orderRepository.save(order);
		return order;
	}

}
