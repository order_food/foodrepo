package com.foodbytes.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.foodbytes.model.Order;
import com.foodbytes.service.IOrderService;

@RestController
public class OrderController {

	
	@Autowired
	IOrderService iOrderService;

	@RequestMapping("/add-to-cart/{customerId}/{itemId}/{quantity}")
	public Order addToCart(@PathVariable("customerId") Integer customerId, @PathVariable("itemId") int itemId,
			@PathVariable("quantity") Integer quantity) {
		return iOrderService.addToCart(customerId, itemId, quantity);
	}

	@RequestMapping("/getOrder-by-customerId/{customerId}/")
	public List<Order> gettOrdersByCustomerId(@PathVariable("customerId") Integer customerId) {
		return iOrderService.gettOrdersByCustomerId(customerId);
	}

	@RequestMapping("/update-cart/{cartId}/{quantity}")
	public Order updateCart(@PathVariable("cartId") int cartId, @PathVariable("quantity") Integer quantity) {
		return iOrderService.updateCart(cartId, quantity);
	}

	@RequestMapping("/delete-by-orderId/{orderId}")
	public Order deleteOrderById(@PathVariable("orderId") Integer orderId) {
		return iOrderService.deleteOrderById(orderId);
	}

	@RequestMapping("/delete-cart-by-cartId/{cartId}")
	public Order deleteCartById(@PathVariable("cartId") int cartId) {
		return iOrderService.deleteCartById(cartId);

	}

	@RequestMapping("/getorder-by-id/{orderId}")
	public Order getOrderById(@PathVariable("orderId") Integer orderId) {
		return iOrderService.getOrderById(orderId);

	}
	@RequestMapping("/checkout/{customerId}/{address}")
	public Order checkOut(@PathVariable("customerId") Integer customerId, @PathVariable("address") String address) {
		return iOrderService.checkOut(customerId, address);

	}

}
