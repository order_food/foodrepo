package com.foodbytes.model;

import javax.persistence.Entity;


public class Menus {
	
	private int itemId;
	private String itemName;
	private float price;
	private String type;

	private Hotel hotel;

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Menus(int itemId, String itemName, float price, String type, Hotel hotel) {
		super();
		this.itemId = itemId;
		this.itemName = itemName;
		this.price = price;
		this.type = type;
		this.hotel = hotel;
	}

	@Override
	public String toString() {
		return "Menus [itemId=" + itemId + ", itemName=" + itemName + ", price=" + price + ", type=" + type + ", hotel="
				+ hotel + "]";
	}

	public Menus() {
		super();
		// TODO Auto-generated constructor stub
	}


	
	
	
	
}
