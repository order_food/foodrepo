package com.foodbytes.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
 
@Entity
@Table(name="orderstable")
public class Order {

	@Id
	@GeneratedValue(generator = "seq")
	@GenericGenerator(
	        name = "seq",
	        strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
	        parameters = {
	                @Parameter(name = "sequence_name", value = "mysequence"),
	                @Parameter(name = "initial_value", value = "1"),
	                @Parameter(name = "increment_size", value = "1")
	        }
	)
	private Integer orderId;
	private String address;
	private double subTotal;
	private Integer hotelId;
	private Integer customerId;
	private String status;
	

	@OneToMany(mappedBy="order",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<Cart> cartList;
	
	
	public Order() {
		super();
	}
	
	


	public Order(Integer orderId, String address, double subTotal, Integer hotelId, Integer customerId, String status,
			List<Cart> cartList) {
		super();
		this.orderId = orderId;
		this.address = address;
		this.subTotal = subTotal;
		this.hotelId = hotelId;
		this.customerId = customerId;
		this.status = status;
		this.cartList = cartList;
	}




	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public double getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public List<Cart> getCartList() {
		return cartList;
	}

	public void setCartList(List<Cart> cartList) {
		this.cartList = cartList;
	}
	public Integer getHotelId() {
		return hotelId;
	}

	public void setHotelId(Integer hotelId) {
		this.hotelId = hotelId;
	}




	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", address=" + address + ", subTotal=" + subTotal + ", hotelId=" + hotelId
				+ ", customerId=" + customerId + ", status=" + status + ", cartList=" + cartList + "]";
	}




	
	
	
	
}
