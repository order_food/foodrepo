package com.foodbytes.model;

import javax.persistence.Entity;


public class Customer {
	
	
	private Integer customerId;
	private String name;
	

	private String email;
	private String password;
	
	private Long mobileNo;
	
	
	public Customer() {
		super();
	}


	public Customer(Integer customerId, String name, String email, String password, Long mobileNo) {
		super();
		this.customerId = customerId;
		this.name = name;
		this.email = email;
		this.password = password;
		this.mobileNo = mobileNo;
	}


	public Integer getCustomerId() {
		return customerId;
	}


	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Long getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}


	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", name=" + name + ", email=" + email + ", password=" + password
				+ ", mobileNo=" + mobileNo + "]";
	}
	
	
	
	
	

}

