/**
 * 
 */
package com.foodbytes.feignclient;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.foodbytes.model.Menus;


/**
 * @author harsriva1
 *
 */
@FeignClient(value="foodbites-hotel-service" ,fallback=IOrderClientImpl.class)
public interface IOrderClient {
	
	@RequestMapping(value="/one-menu/{ItemId}")
	public Menus showMenus(@PathVariable("ItemId") int id);
	
}
