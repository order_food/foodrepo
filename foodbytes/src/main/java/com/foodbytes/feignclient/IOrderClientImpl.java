/**
 * 
 */
package com.foodbytes.feignclient;



import org.springframework.stereotype.Service;

import com.foodbytes.model.Menus;


/**
 * @author harsriva1
 *
 */
@Service
public class IOrderClientImpl implements IOrderClient{

	@Override
	public Menus showMenus(int id) {
		// TODO Auto-generated method stub
		System.out.println("fallback method to be called for searching menus");
		return new Menus();
	}
}
