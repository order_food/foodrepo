/**
 * 
 */
package com.foodbytes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foodbytes.model.Order;

/**
 * @author harsriva1
 *
 */
@Repository
public interface OrderRepository extends JpaRepository<Order,Integer>{
	
	List<Order> findByCustomerId(Integer customerId);
	List<Order> findByStatusAndCustomerId(String status,Integer customerId);
	
	

}
