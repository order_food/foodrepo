/**
 * 
 */
package com.foodbytes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foodbytes.model.Cart;


/**
 * @author harsriva1
 *
 */
@Repository
public interface CartRepository extends JpaRepository<Cart,Integer>{
	List<Cart> findByOrderId(Integer orderId);

}
